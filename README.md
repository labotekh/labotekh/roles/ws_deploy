# `labotekh.labotekh.ws_deploy`
The `labotekh.labotekh.ws_deploy` role deploy Docker images of the service on the host.

The `docker-compose.yml` placed in the configuration folder of the service is used as a base to deploy the Docker images. You can find below an example for a simple Django project. Note that all variables defined in the `services_variables_file` are usable with Jinja2 syntax as well as the variables required for all roles of the collection.

```yml
version: "3.9"

services:
  db:
    image: postgres:latest
    restart: always
    volumes:
      - {{ root_directory }}/{{ item.value.service_name }}/data:/var/lib/postgresql/data
    env_file:
      - {{ item.value.service_name }}.env
    networks:
      - db_link

  web:
    image: {{ item.value.registry_image }}:latest
    restart: always
    entrypoint: /code/entrypoint_master.sh
    command: gunicorn liam.wsgi:application -b :8000
    volumes:
      - {{ root_directory }}/{{ item.value.service_name }}/files:/code/files:rw
    env_file:
      - {{ item.value.service_name }}.env
    depends_on:
      - db
    networks:
      - nginx_link
      - db_link

networks:
  db_link:
    driver: bridge
  nginx_link:
    driver: bridge
```

The role will also generate a `.env` file at the same location as the `docker-compose.yml` with the following variables :
```env
# calculated service FQDN
PRODUCTION_DOMAIN=<fqdn>

# those can be provided as role vars aren't set if not provided
DEFAULT_ADMIN_USERNAME=<default_admin_username>
DEFAULT_ADMIN_PASSWORD=<default_admin_password>
DEFAULT_LOG_LEVEL=INFO

# if database password is provided
POSTGRES_DB=<serive_name>_db
POSTGRES_USER=<serive_name>_exp
POSTGRES_PASSWORD=<db_password>
POSTGRES_HOST=db
POSTGRES_PORT=5432
PGDATA=/var/lib/postgresql/data/<serive_name>

# here you will find the custom environment variables provided in your <service_variables_file>
```

## Tips

Your main Docker image should listen on port 8000 in order to receive Web requests from the reverse proxy.

Don't forget to use the `files` directory to store your static and media files which will be served by the reverse proxy.
